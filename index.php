<?php

class YandexPayment
{

    /**
     * @param $answer
     * @return array|bool
     */
    public function getValuesFromYandexAnswer($answer)
    {
        $values = [];
        // яндекс кошелек от 11 до 20 цифр. код подтверждения такой длинны не может быть
        $answer = preg_replace_callback('^([1-9])+(?:-?\d){11,20}^',
            function ($matches) use (&$values) {
                $values['number'] = $matches[0];
                return '';
            },
            $answer);
        // сумма число с плавающей точкой
        $answer = preg_replace_callback('^[0-9]*[.,][0-9]+^',
            function ($matches) use (&$values) {
                $values['cash'] = $matches[0];
                return '';
            },
            $answer);
        // оставщееся это код. поэтому так вырезал, он не нормирован, но 11 цифр - точно не будет
        preg_replace_callback('^[0-9]+^',
            function ($matches) use (&$values) {
                $values['code'] = $matches[0];
                return '';
            },
            $answer);

        return  isset($values['code']) && isset($values['cash']) && isset($values['number']) ? $values :  false ;
    }
}
